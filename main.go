package main

import (
	"ConcurrencyCase/src/service"
	"flag"
)

func main() {
	directory := ""
	key := 0
	flag.StringVar(&directory, "dir", "", "Path of Directory")
	flag.IntVar(&key, "key", 5, "K from question")
	flag.Parse()

	//it is basic useage
	if directory == "" {
		panic("Invalid dataset path !")
	}

	managerService := service.NewManagerService(directory, key)
	managerService.Process()

}
