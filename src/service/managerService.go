package service

import (
	"fmt"
	"os"
	"runtime"
	"strings"
	"sync"
)

type ManagerService struct {
	Readers       []*ReaderService
	CurrentWorker int
	WriterService WriterService
	ReadedFileNum int
	FileNum       int
	DataChan      chan []string
	wg            *sync.WaitGroup
	Done          chan bool
	ReaderDone    chan bool
}

func NewManagerService(RawDirPath string, k int) ManagerService {
	m := initManager()
	m.InitReader(RawDirPath)
	m.InitWriter(k, RawDirPath)

	return m
}
func (m *ManagerService) Process() {

	go m.StartReader()
	m.WriterService.Listen()

}
func initManager() ManagerService {
	return ManagerService{
		DataChan:      make(chan []string, 1),
		Done:          make(chan bool, 1),
		ReaderDone:    make(chan bool, 1),
		wg:            &sync.WaitGroup{},
		FileNum:       0,
		ReadedFileNum: 0,
		CurrentWorker: 0,
	}
}

func (m *ManagerService) InitReader(RawDirPath string) {
	m.Readers = make([]*ReaderService, 0)
	rawDir := fmt.Sprintf("%s/raw", RawDirPath)
	// Open the directory.
	inputDirRead, _ := os.Open(rawDir)

	// Call Readdir to get all files.
	inputDirFiles, _ := inputDirRead.Readdir(0)

	// Loop over files.
	for inputIndex := range inputDirFiles {
		inputFileHere := inputDirFiles[inputIndex]

		// Get name of file.
		inputNameHere := inputFileHere.Name()

		//basic check
		if strings.Contains(inputNameHere, ".csv") {
			m.FileNum++
			filePATH := fmt.Sprintf("%v/%v", rawDir, inputNameHere)
			m.Readers = append(m.Readers, NewReaderService(filePATH, m))
		}
	}
}
func (m *ManagerService) GoSlaveGo(id int, wg *sync.WaitGroup) {
	reader := m.Readers[id]
	m.CurrentWorker++
	go reader.ReadAndSend(wg)
}
func (m *ManagerService) StartReader() {

	cores := runtime.NumCPU()
	// fmt.Println(m.FileNum)
	var wg sync.WaitGroup
	wg.Add(m.FileNum)

	for i := 0; i < cores; i++ {
		//basic check if cpu cores are bigger than file num
		if m.FileNum == m.CurrentWorker {
			break
		}

		m.GoSlaveGo(m.CurrentWorker, &wg)
	}
loop:
	for {
		select {
		case <-m.Done:
			m.ReadedFileNum++
			if m.ReadedFileNum == m.FileNum {
				break loop
			}
			if m.ReadedFileNum+m.CurrentWorker < m.FileNum {
				m.GoSlaveGo(m.ReadedFileNum+m.CurrentWorker, &wg)
			}
			m.CurrentWorker--
		}
	}

	wg.Wait()
	//eventual consistency
	m.ReaderDone <- true

}

func (m *ManagerService) InitWriter(RowCount int, RawDirPath string) {
	//fixme:bad usage
	m.WriterService = NewWriterService(RowCount, m, RawDirPath, m.Readers[0].HeaderData)
}
