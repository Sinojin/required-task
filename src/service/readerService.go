package service

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"sync"
)

type ReaderService struct {
	filePath   string
	HeaderData []string
	M          *ManagerService
}

func NewReaderService(filePath string, ManagerService *ManagerService) *ReaderService {

	r := ReaderService{filePath: filePath, M: ManagerService}
	f, err := os.Open(r.filePath)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	rd := bufio.NewReader(f)
	line, err := rd.ReadString('\r')
	r.HeaderData = r.lineToStrArray(line)
	return &r
}

func (r *ReaderService) ReadAndSend(wg *sync.WaitGroup) {
	f, err := os.Open(r.filePath)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	rd := bufio.NewReader(f)
	//first row
	line, err := rd.ReadString('\r')
	for {
		line, err = rd.ReadString('\r')
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatalf("read file line error: %v", err)
			return
		}
		r.M.DataChan <- r.lineToStrArray(line)
	}

	//it is done send to service
	r.M.Done <- true
	wg.Done()

}

func (r *ReaderService) lineToStrArray(line string) []string {
	line = strings.TrimSuffix(line, "\r")
	return strings.Split(line, ",")
}
