package service

import (
	"encoding/csv"
	"fmt"
	"os"
)

type WriterService struct {
	CurrentFileNum int
	MaxRowCount    int
	FolderPath     string
	M              *ManagerService
	Header         []string
	FileContent    int
	writer         *csv.Writer
	f              *os.File
}

func NewWriterService(MaxRowCount int, M *ManagerService, FolderPath string, Header []string) WriterService {
	FolderPath = fmt.Sprintf("%v/output/merged", FolderPath)
	os.RemoveAll(FolderPath)
	os.MkdirAll(FolderPath, 0700)
	return WriterService{MaxRowCount: MaxRowCount, M: M, FolderPath: FolderPath, Header: Header, FileContent: 0}
}

func (s *WriterService) Listen() {
	//because we trust eventual consistency :)
	for {
		select {
		case row := <-s.M.DataChan:
			s.AddOrCreateNewRow(row)
		case <-s.M.ReaderDone:
			return
		}
	}
}

func (s *WriterService) AddOrCreateNewRow(row []string) {
	if s.FileContent == s.MaxRowCount || s.FileContent == 0 {
		s.NewFile()
	}
	s.WriteToFile(row)
}

func (s *WriterService) NewFile() {
	if s.f != nil {
		s.f.Close()
	}
	fileName := fmt.Sprintf("base_part%v.csv", s.CurrentFileNum)
	//no error because it is best case (:
	s.f, _ = os.Create(fmt.Sprintf("%v/%v", s.FolderPath, fileName))
	s.writer = csv.NewWriter(s.f)
	s.WriteToFile(s.Header)
	s.FileContent = 1
	s.CurrentFileNum++
}

func (s *WriterService) WriteToFile(row []string) {
	s.writer.Write(row)
	s.writer.Flush()
	s.FileContent++
}
